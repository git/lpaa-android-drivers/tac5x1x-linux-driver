ifneq ($(KERNELRELEASE),)
snd-soc-tac5x1x-objs := tac5x1x.o tac5x1x-i2c.o
obj-m += snd-soc-tac5x1x.o
else
KDIR := $(Kernel Path)
PWD  :=$(shell pwd)
all:
	make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- -C $(KDIR) M=$(PWD) modules
clean:
	rm -f *.ko *.o *.mod.o *.symvers *.cmd  *.mod.c *.order
endif
